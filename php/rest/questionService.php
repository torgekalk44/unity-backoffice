<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Authorization');
include('./config/config.php');


if (isset($_GET['getAllQuestionsForSurvey'])) {
  if (isset($_GET["surveyId"])) {
    $surveyId = mysqli_real_escape_string($con, $_GET["surveyId"]);

    $getAllQuestions = mysqli_query($con, 'SELECT `id`, `label`, `description`, `question_order` FROM `questions` WHERE `survey_id` = ' . $surveyId . ' ORDER BY `question_order`');

    $jsonResult = array();

    while ($row = mysqli_fetch_assoc($getAllQuestions)) {
      array_push($jsonResult, $row);
    }

    echo json_encode($jsonResult);
  }

}

if (isset($_GET['deleteQuestion'])) {
  $data = json_decode(file_get_contents('php://input'), true);
  $result = new \stdClass();

  if (!IsNullOrEmptyString($data['id'])) {
    $questionId = $con->real_escape_string($data['id']);
    echo $questionId;

    if ($con->query('DELETE FROM `questions` WHERE `id` = ' . $questionId)) {
      $result->code = 200;
      $result->text = 'Question deleted: ' . $questionId;
    } else {
      $result->code = 500;
      $result->text = $con->error;
      $result->error = $con->errno;
    }
  }
}

if (isset($_GET['postQuestion'])) {
  $data = json_decode(file_get_contents('php://input'), true);
  $result = new \stdClass();

  if (!IsNullOrEmptyString($data['label']) and !IsNullOrEmptyString($data['description']) and !IsNullOrEmptyString($data['question_order']) and !IsNullOrEmptyString($data['survey_id'])) {
    $label = $con->real_escape_string($data['label']);
    $description = $con->real_escape_string($data['description']);
    $questionOrder = $con->real_escape_string($data['question_order']);
    $surveyId = $con->real_escape_string($data['survey_id']);

    if ($con->query('INSERT INTO `questions` (`label`, `description`, `question_order`, `survey_id`) VALUES ("' . $label . '", "' . $description . '", ' . $questionOrder . ', ' . $surveyId . ')')) {
      $result->code = 200;
      $result->text = 'Question added: ' . $label;
    } else {
      $result->code = 500;
      $result->text = $con->error;
      $result->error = $con->errno;
    }
  }
}

function IsNullOrEmptyString($str)
{
  return (!isset($str) || trim($str) === '');
}
?>