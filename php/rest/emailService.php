<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type');
if (isset($_GET["sendMail"])) {

    $data = json_decode(file_get_contents('php://input'), true);

    if ($data["protection"] == "") {
        $surveyId = $data["surveyId"];
        $survey = $data["survey"];
        $company = $data["company"];
        $email = $data["email"];
        $name = $data["name"];
        $telephone = $data["phone"];

        try {
            $headers = "From: " . utf8_decode($name) . "<" . $email . ">" . PHP_EOL;
            $headers .= "MIME-Version: 1.0" . PHP_EOL;
            $headers .= "Content-Type: text/html; charset=iso-8859-1" . PHP_EOL;
            $headers .= "Content-Transfer-Encoding: 8bit" . PHP_EOL;

            if ($survey and $email and $name and $company) {
                mail("torgekalk44@gmail.com", ($survey . " wurde ausgefüllt"), "
                <p>Name: " . utf8_decode($name) . "</p>
                <p>E-Mail: " . utf8_decode($email) . "</p>
                <p>Telefonnummer: " . utf8_decode($telephone) . "</p>
                <p>Firma: " . utf8_decode($company) . "</p>", $headers);
                echo $surveyId;
            }
        } catch (Exception $e) {
            echo $e;
        }
    }
}
