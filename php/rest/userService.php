<?php
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
  header('Access-Control-Allow-Headers: Content-Type, Authorization');
  include('./config/config.php');
  $options = array('cost' => 8);

  if (isset($_GET['login'])) {
    include('./config/jwt.php');
    echo login($con, json_decode(file_get_contents('php://input'), true));
  }

  if (isset($_GET['refresh'])) {
    include('./config/jwt.php');
    echo json_encode(refreshToken(json_decode(file_get_contents('php://input'), true)['jwt']));
  }

  function IsNullOrEmptyString($str){
    return (!isset($str) || trim($str) === '');
  }
?>