<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Authorization');
include('./config/config.php');


if (isset($_GET['postAnswers'])) {
  $data = json_decode(file_get_contents('php://input'), true);
  $result = new \stdClass();

  if (isset($data['answers'])) {
    $attendee = $data['attendee'];
    $answers = $data['answers'];
   


    $attendeeQuery = $con->query('INSERT INTO `attendee` (`name`, `email`, `phone`, `company`) VALUES ("' . $con->real_escape_string($attendee['name']) . '", "' . $con->real_escape_string($attendee['email']) . '", "' . $con->real_escape_string($attendee['phone']) . '", "' . $con->real_escape_string($attendee['company']) . '")');
    $attendeeId = $con->insert_id;
    
    foreach ($answers as $key => $answer) {
      $question = $answer['question'];
      $category = $answer['category'];
      
      $con->query('INSERT INTO `answer` (`attendee_id`, `survey_id`, `category_id`, `question_id`, `question_label`, `answer`) VALUES ("' . $attendeeId . '", "' . $con->real_escape_string($category['survey_id']) . '", "' . $con->real_escape_string($category['id']) . '", "' . $con->real_escape_string($question['id']) . '", "' . $con->real_escape_string($question['label']) . '", "' . $con->real_escape_string($answer['answer']) . '")');
      $answerId = $con->insert_id;
    }

    if ($answerId) {
      echo $category['survey_id'];
    }
  }
}

if (isset($_GET['getAnswers'])) {
  if (isset($_GET['surveyId'])) {
    $getAnswers = mysqli_query($con, 'SELECT `attendee_id`, `survey_id`, `category_id`, `question_id`, `question_label`, `answer` FROM `answer` WHERE `survey_id` = ' . $con->real_escape_string($_GET['surveyId']));
    $getAttendeeCount = mysqli_query($con, 'SELECT count(distinct `attendee_id`) as attendees FROM `answer` WHERE `survey_id` = ' . $con->real_escape_string($_GET['surveyId']));
    $getAttendeeCountRow = mysqli_fetch_assoc($getAttendeeCount);
    
    $jsonRow['attendees'] = intval($getAttendeeCountRow);
    
    $jsonResult = array();    
    while ($answerRow = mysqli_fetch_assoc($getAnswers)) {
      $categoryId = mysqli_real_escape_string($con, $answerRow['category_id']);
      $questionId = mysqli_real_escape_string($con, $answerRow['question_id']);
      $answerRow['answer'] = intval($answerRow['answer']);

      $getCategory = mysqli_query($con, 'SELECT `id`, `name` FROM `categories` WHERE `id` = ' . $categoryId);
    
      while ($categoryRow = mysqli_fetch_assoc($getCategory)) {
        $getQuestionCount = mysqli_query($con, 'SELECT count(*) as total FROM `questions` WHERE `survey_id` = ' . $con->real_escape_string($_GET['surveyId']) . ' AND `category_id` = ' . $categoryId);
        $getQuestionCountRow = mysqli_fetch_assoc($getQuestionCount);
        $categoryRow['questionCount'] = $getQuestionCountRow['total'];
        $answerRow['category'] = $categoryRow;
        unset($answerRow['category_id']);
        unset($answerRow['attendee_id']);
        unset($answerRow['question_id']);
      }
      
      $getQuestion = mysqli_query($con, 'SELECT `label`, `question_order` as questionOrder FROM `questions` WHERE `id` = ' . $questionId);
    
      while ($questionRow = mysqli_fetch_assoc($getQuestion )) {
        $answerRow['question'] = $questionRow;
        unset($answerRow['question_id']);
      }
        
      array_push($jsonResult, $answerRow);
    }

    $jsonRow['answers'] = $jsonResult;
    echo json_encode($jsonRow);
  }
}

function IsNullOrEmptyString($str)
{
  return (!isset($str) || trim($str) === '');
}
 