<?php 
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Authorization');
include('./config/config.php');


if (isset($_GET['getAllSurveys'])) {

  $getAllSurveys = mysqli_query($con, 'SELECT `id`, `name` FROM `surveys`');

  $jsonResult = array();

  while ($row = mysqli_fetch_assoc($getAllSurveys)) {
    $getQuestions = mysqli_query($con, 'SELECT `id`, `label`, `question_order` as questionOrder FROM `questions` WHERE `survey_id` = ' . $con->real_escape_string($row['id']));
    $jsonQuestions = array();

    while ($questionRow = mysqli_fetch_assoc($getQuestions)) {
      array_push($jsonQuestions, $questionRow);
    }

    $row['questions'] = json_encode($jsonQuestions);
    array_push($jsonResult, $row);
  }

  echo json_encode($jsonResult);
}

if (isset($_GET['getSurvey'])) {
  if (isset($_GET['surveyId'])) {
    $getSurvey = mysqli_query($con, 'SELECT `id`, `name` FROM `surveys` WHERE `id` = ' . $con->real_escape_string($_GET['surveyId']));

    $jsonResult = array();

    $getCategories = mysqli_query($con, 'SELECT `id`, `name`, `survey_id` FROM `categories` WHERE `survey_id` = ' . $con->real_escape_string($_GET['surveyId'] ));
    $jsonCategories = array();

    $row = mysqli_fetch_assoc($getSurvey);

    while ($categoryRow = mysqli_fetch_assoc($getCategories)) {
      $getQuestions = mysqli_query($con, 'SELECT `id`, `label`, `question_order` as questionOrder, `category_id` FROM `questions` WHERE `survey_id` = ' . $con->real_escape_string($_GET['surveyId'] .' AND `category_id` = '. $categoryRow['id']));
      $jsonQuestions = array();

      while ($questionRow = mysqli_fetch_assoc($getQuestions)) {
        array_push($jsonQuestions, $questionRow);
      }
      $categoryRow['questions'] = $jsonQuestions;
    
      array_push($jsonCategories, $categoryRow);
    }
    
    $row['categories'] = $jsonCategories;  
    echo json_encode($row);
  }
}


if (isset($_GET['postSurvey'])) {
  $data = json_decode(file_get_contents('php://input'), true);
  $result = new \stdClass();

  if (!IsNullOrEmptyString($data['name'])) {
    $name = $con->real_escape_string($data['name']);

    $categories = $data['categories'];
    

    if (!isset($data['id'])) {
      $surveyQuery = $con->query('INSERT INTO `surveys` (`name`) VALUES ("' . $name . '")');
      $surveyId = $con->insert_id;
    } else {
      $surveyId = $con->real_escape_string($data['id']);
      $surveyQuery = $con->query('UPDATE `surveys` SET `name` = "' . $name . '" WHERE `id` = ' . $surveyId);
    }

    foreach ($categories as $key => $category) {
      if (isset($category['isNew']) && $category['isNew']) {
        $con->query('INSERT INTO `categories` (`name`, `survey_id`) VALUES ("' . $con->real_escape_string($category['name']) . '", ' . $surveyId . ')');
        $categoryId = $con->insert_id;
      } else if (isset($category['isDeleted']) && $category['isDeleted'] && (!isset($category['isNew']) || !$category['isNew'])) {
        $con->query('DELETE `categories` WHERE id = ' . $con->real_escape_string($category['id']) . ')');
      } else {
        $con->query('UPDATE `categories` SET `name` = "' . $con->real_escape_string($category['name']) . '" WHERE `id` = ' . $con->real_escape_string($category['id']));
        $categoryId = $con->real_escape_string($category['id']);
        echo $con->error;
      }
      $order = 1;
      $questions = $category['questions'];
      foreach ($questions as $key => $question) {
        if (isset($question['isNew']) && $question['isNew']) {
          $con->query('INSERT INTO `questions` (`label`, `question_order`, `survey_id`, `category_id`) VALUES ("' . $con->real_escape_string($question['label']) . '", "' . $order . '", ' . $surveyId . ', "' . $categoryId . '")');
        } else if (isset($question['isDeleted']) && $question['isDeleted'] && (!isset($question['isNew']) || !$question['isNew'])) {
          $con->query('DELETE `questions` WHERE id = ' . $con->real_escape_string($question['id']) . ')');
        } else {
          $con->query('UPDATE `questions` SET `label` = "' . $con->real_escape_string($question['label']) . '", `question_order` = ' . $order . ' WHERE `id` = ' . $con->real_escape_string($question['id']));
          echo $con->error;
        }
  
        $order++;
      }
    }

    if ($surveyQuery) {
      echo $surveyId;
    }
  }
}

function IsNullOrEmptyString($str)
{
  return (!isset($str) || trim($str) === '');
}
 