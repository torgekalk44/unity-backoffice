import * as ReactDOM from 'react-dom';
import * as React from 'react';
import { Main } from './components/Main/Main';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Navigation } from './components/Navigation/Navigation';
import { initializeIcons } from '@uifabric/icons';
import { SurveyForm } from './components/Surveys/SurveyForm/SurveyForm';
import './App.scss';
import { SurveyList } from './components/Surveys/SurveyList/SurveyList';
initializeIcons();

ReactDOM.render(
  <BrowserRouter>
    <React.Fragment>
      <Navigation />
      <Switch>
        <Route exact path='/' component={Main}></Route>
        <Route exact path='/viewSurveys' component={SurveyList}></Route>
        <Route exact path='/addSurvey' component={SurveyForm}></Route>
        <Route exact path='/editSurvey/:surveyId' component={SurveyForm}></Route>
      </Switch>
    </React.Fragment>
  </BrowserRouter>,
  document.getElementById('app'));