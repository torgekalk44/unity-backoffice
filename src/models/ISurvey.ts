import { IQuestion } from './IQuestion';
import { ICategory } from './ICategory';

export interface ISurvey {
  id?: number;
  name: string;
  questions?: IQuestion[];
  categories: ICategory[];
}