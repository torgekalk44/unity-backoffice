export interface IQuestion {
  id: number | string;
  label: string;
  description: string;
  questionOrder: number;
  category_id: number | string;
  isNew?: boolean;
  isDeleted?: boolean;
}