import { IQuestion } from "./IQuestion";

export interface ICategory {
  id?: number | string;
  name?: string;
  questions: IQuestion[];
  survey_id?: number;
  isNew?: boolean;
  isDeleted?: boolean;
}