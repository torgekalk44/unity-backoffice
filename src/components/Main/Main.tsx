import * as React from 'react';
import * as styles from './Main.module.scss';
import { css } from '@uifabric/utilities';
import { ActionButton } from 'office-ui-fabric-react/lib/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

export const Main = (): JSX.Element => {
  return (
    <>
      <div className='ms-Grid' dir='ltr'>
        <div className='ms-Grid-row'>
          <div className='ms-Grid-col ms-sm12'><h1 className='ms-font-su'>Backoffice - Umfragen</h1></div>
        </div>
        <div className='ms-Grid-row'>
          <div className='ms-Grid-col ms-sm6'>
            <a href='/addSurvey' className={styles.link}>
              <Card className={styles.unityBtn} classes={{ root: styles.card }}>
                <CardContent>
                  Neue Umfrage anlegen
                </CardContent>
                <CardActions>
                  <ActionButton data-automation-id='add' iconProps={{ iconName: 'AddFriend' }} allowDisabledFocus={true}>
                    Anlegen
                  </ActionButton>
                </CardActions>
              </Card>
            </a>
          </div>
          <div className='ms-Grid-col ms-sm6'>
            <a href='/viewSurveys' className={styles.link}>
              <Card className={styles.unityBtn} classes={{ root: styles.card }}>
                <CardContent>
                  Alle Umfragen anzeigen
                </CardContent>
                <CardActions>
                  <ActionButton data-automation-id='edit' iconProps={{ iconName: 'edit' }} allowDisabledFocus={true} href='/viewSurveys'>
                    Anzeigen
                </ActionButton>
                </CardActions>
              </Card>
            </a>
          </div>
        </div>
      </div>
    </>
  );
};