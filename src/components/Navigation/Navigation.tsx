import * as React from 'react';

import { CommandBar, ICommandBarItemProps } from 'office-ui-fabric-react/lib/CommandBar';
import { withRouter } from 'react-router';

export const Navigation = withRouter(({ history }): JSX.Element => {
    const items: ICommandBarItemProps[] = [
        {
            key: 'surveys',
            name: 'Umfragen',
            iconProps: {
                iconName: 'CustomList'
            },
            subMenuProps: {
                items: [{
                    key: 'viewSurveys',
                    name: 'Umfragen anzeigen',
                    iconProps: {
                        iconName: 'CustomList'
                    },
                    onClick: (): void => history.push('/viewSurveys')
                },
                {
                    key: 'addSurvey',
                    name: 'Umfragen hinzufügen',
                    iconProps: {
                        iconName: 'Add'
                    },
                    onClick: (): void => history.push('/addSurvey')
                }]
            }
        }
    ];
    return (
        <CommandBar items={items}></CommandBar>
    );
});