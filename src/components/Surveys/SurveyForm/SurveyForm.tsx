import * as React from 'react';
import { ISurveyFormState, ISortableListProps, ISortableItemProps } from './ISurveyForm';
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import { SurveyService } from '../../../services/SurveyService';
import { PrimaryButton, Button, IconButton } from 'office-ui-fabric-react/lib/Button';
import { IQuestion } from '../../../models/IQuestion';
import { v4 as uuid } from 'uuid';
import { RouteComponentProps } from 'react-router-dom';
import { LoadingOverlay } from '../../LoadingOverlay/LoadingOverlay';
import * as styles from './SurveyForm.module.scss';
import {
    SortableContainer,
    SortableElement,
    arrayMove,
    SortEnd,
    SortableHandle
} from 'react-sortable-hoc';
import { css } from '@uifabric/utilities';
import { ICategory } from '../../../models/ICategory';

const DragHandle = SortableHandle(() =>
    <IconButton iconProps={{ iconName: 'ChevronUnfold10' }}></IconButton>
);

const SortableItem = SortableElement((props: ISortableItemProps) =>

    <div className={css('ms-Grid-row', styles.questionRow)}>
        <TextField className='ms-Grid-col ms-sm10' key={'input' + props.item.label} value={props.item.label} onBeforeChange={props.onQuestionInputChange(props.item.id, props.item.category_id)} />
        <DragHandle key={'drag-' + props.item.label} />
        <IconButton className='ms-Grid-col ms-sm1' key={'delete-' + props.item.label} iconProps={{ iconName: 'delete' }} onClick={(): void => props.deleteQuestion(props.item)}></IconButton>
    </div>);

const SortableList = SortableContainer((props: ISortableListProps) => {
    return (
        <ul>
            {props.items.map((value: IQuestion, index: number) => (
                <SortableItem key={`item-${index}`} index={index} item={value} {...props} />
            ))}
        </ul>
    );
});

export class SurveyForm extends React.Component<RouteComponentProps<any>, ISurveyFormState> {
    private readonly _surveyService: SurveyService = new SurveyService();
    constructor(props: RouteComponentProps<any>, state: ISurveyFormState) {
        super(props);
        this.state = {
            ...state, survey: { name: '', categories: [] }
        };
    }

    public componentDidMount(): void {
        if (this.props.match.params.surveyId) {
            this.getSurvey(this.props.match.params.surveyId);
        }
    }

    public render(): JSX.Element {
        return (
            <React.Fragment>
                <LoadingOverlay show={this.state.isLoading} text='Speichere...' />
                <h1 className='ms-font-su'>Umfrage "{this.state.survey.name}" {this.props.match.params.surveyId ? 'bearbeiten' : 'hinzufügen'}</h1>
                <form key={'form'} onSubmit={this.onSave}>
                    <TextField label='Name' value={this.state.survey.name} required onBeforeChange={this.onSurveyInputChange('name')} />
                    <hr />
                    <TextField label='Kategorie' value={this.state.newCategory} placeholder='Kategorie eingeben' onBeforeChange={this.onInputChange('newCategory')} />
                    <PrimaryButton text='Kategorie hinzufügen' onClick={this.onAddCategory} />
                    <hr />
                    {this.state.survey.categories.map((category: ICategory): JSX.Element =>
                        <div key={category.name} className={styles.category}>
                            <div className='ms-Grid-row' key={category.name}>
                                <div className='ms-Grid-col ms-sm8'>Kategorie: {category.name}</div>
                                <IconButton key={'delete-' + category.name} className='ms-Grid-col ms-sm1' iconProps={{ iconName: 'delete' }} ></IconButton>
                            </div>
                            <TextField key={'question-' + category.name} label='Question' value={this.state.newQuestion} placeholder='Frage eingeben' onBeforeChange={this.onInputChange('newQuestion')} />
                            <PrimaryButton key={'newQuestion-' + category.name} text='Frage hinzufügen' onClick={(): void => this.onAddQuestion(category)} />
                            <SortableList key={'list-' + category.name} useDragHandle items={this.state.survey.categories.filter(q => q.id === category.id)[0].questions} onQuestionInputChange={this.onQuestionInputChange} deleteQuestion={this.deleteQuestion} onSortEnd={this.onSortEnd(category)} />
                        </div>
                    )}
                    <PrimaryButton text='Speichern' type='submit' />
                </form>
            </React.Fragment>

        );
    }

    private onInputChange = (name: string): (newValue: string) => void => (newValue: string): void => {
        this.setState({ ...this.state, [name]: newValue });
    }

    private onSurveyInputChange = (name: string): (newValue: string) => void => (newValue: string): void => {
        this.setState({ survey: { ...this.state.survey, [name]: newValue } });
    }

    private onQuestionInputChange = (id: number | string, categoryId: number | string): (newValue: string) => void => (newValue: string): void => {
        const { categories } = this.state.survey;
        const { questions } = this.state.survey.categories.filter(q => q.id === categoryId)[0];
        const category = categories.find(q => q.id === categoryId);
        const question = questions.find(q => q.id === id);
        if (category && question) {
            questions[questions.findIndex(q => q.id === question.id)] = { ...question, label: newValue };
        }
    }

    private onSave = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
        this.setState({ isLoading: true });
        e.preventDefault();
        try {
            const id = (await this._surveyService.saveSurvey(this.state.survey)).data;
            if (id && !this.props.match.params.surveyId) {
                window.location.href = '/editSurvey/' + id;
            }
        } catch (error) {
            console.error(error);
        } finally {
            this.setState({ isLoading: false });
        }
    }

    private getSurvey = async (id: number): Promise<void> => {
        try {
            const survey = (await this._surveyService.getSurvey(id)).data;
            this.setState({ survey });
        } catch (error) {
            console.error(error);
        }
    }

    private deleteQuestion = async (question: IQuestion): Promise<void> => {
        try {
            await this._surveyService.deleteQuestion(question);
            this.getSurvey(this.state.survey.id);
        } catch (error) {
            console.error(error);
        }
    }

    private onAddQuestion = (thisCategory: ICategory): void => {
        const { categories } = this.state.survey;
        const { questions } = this.state.survey.categories.filter(q => q.id === thisCategory.id)[0];
        const category = categories.find(q => q.id === thisCategory.id);

        if (category) {
            questions.push({
                id: uuid(),
                label: this.state.newQuestion,
                description: '',
                questionOrder: questions.filter(q => q.category_id === thisCategory.id).length + 1,
                category_id: category.id,
                isNew: true
            });
            this.setState({ survey: { ...this.state.survey, categories }, newQuestion: '' });
        }
    }

    private onAddCategory = (): void => {
        const { categories } = this.state.survey;
        categories.push({
            id: uuid(),
            name: this.state.newCategory,
            survey_id: this.state.survey.id,
            isNew: true,
            questions: []
        });
        this.setState({ survey: { ...this.state.survey, categories }, newCategory: '' });
    }

    private onSortEnd = (thisCategory: ICategory): (sortEnd: SortEnd) => void => (sortEnd: SortEnd): void => {
        const { categories } = this.state.survey;
        const category = categories.find(c => c.id === thisCategory.id);
        const question = category;
        if (category) {
            category.questions = arrayMove(category.questions, sortEnd.oldIndex, sortEnd.newIndex);
        }
        this.setState({
            survey: {
                ...this.state.survey, categories
            }
        });
    }
}