import { ISurvey } from '../../../models/ISurvey';
import { IQuestion } from '../../../models/IQuestion';

export interface ISurveyFormState {
  isLoading: boolean;
  survey: ISurvey;
  newCategory: string;
  newQuestion: string;
}

export interface ISortableProps {
  deleteQuestion(question: IQuestion): void;
  onQuestionInputChange(value: string | number, categoryId: string | number): (newValue: any) => void;
}

export interface ISortableItemProps extends ISortableProps {
  item: IQuestion;
}

export interface ISortableListProps extends ISortableProps {
  items: IQuestion[];
}