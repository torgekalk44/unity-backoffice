import { ISurvey } from '../../../models/ISurvey';

export interface ISurveyListState {
    surveys: ISurvey[];
}