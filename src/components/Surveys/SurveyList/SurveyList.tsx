import * as React from 'react';
import { ISurveyListState } from './ISurveyList';
import { RouteComponentProps } from 'react-router';
import { ISurvey } from '../../../models/ISurvey';
import { SurveyService } from '../../../services/SurveyService';
import { element } from 'prop-types';
import { IconButton } from 'office-ui-fabric-react/lib/Button';

export class SurveyList extends React.Component<RouteComponentProps<any>, ISurveyListState> {
    private readonly _surveyService: SurveyService = new SurveyService();
    constructor(props: RouteComponentProps<any>, state: ISurveyListState) {
        super(props);
        this.state = { ...state, surveys: [] };
    }

    public componentDidMount(): void {
        this.getAllSurveys();
    }

    public render(): JSX.Element {
        return (
            <React.Fragment>
                <h1>Umfragen</h1>

                <div className='ms-Grid' dir='ltr'>
                    {this.state.surveys.map((survey: ISurvey): JSX.Element =>
                        <div className='ms-Grid-row' key={survey.id}>
                            <div className='ms-Grid-col ms-sm8'>{survey.name}</div>

                            <IconButton className='ms-Grid-col ms-sm1' iconProps={{ iconName: 'edit' }} href={'/editSurvey/' + survey.id}></IconButton>
                            <IconButton className='ms-Grid-col ms-sm1' iconProps={{ iconName: 'delete' }} ></IconButton>
                        </div>
                    )}
                </div>
            </React.Fragment >
        );
    }

    private getAllSurveys = async (): Promise<void> => {
        try {
            const surveys = (await this._surveyService.getAllSurveys()).data;
            console.debug(surveys);
            this.setState({ surveys });

        } catch (error) {
            console.error(error);
        }
    }
}