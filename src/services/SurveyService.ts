import { ISurvey } from '../models/ISurvey';
import axios, { AxiosPromise } from 'axios';
import { ISurveyListState } from '../components/Surveys/SurveyList/ISurveyList';
import { request } from 'http';
import { IQuestion } from '../models/IQuestion';

export class SurveyService {
    public saveSurvey = (survey: ISurvey): AxiosPromise<number> => {
        const requestUrl = 'http://localhost/rest/surveyService.php?postSurvey';
        return axios.post(requestUrl, survey);
    }

    public getSurvey = (surveyId: number): AxiosPromise<ISurvey> => {
        const requestUrl = `http://localhost/rest/surveyService.php?getSurvey&surveyId=${surveyId}`;
        return axios.get(requestUrl);
    }

    public getAllSurveys = (): AxiosPromise<ISurvey[]> => {
        const requestUrl = 'http://localhost/rest/surveyService.php?getAllSurveys';
        return axios.get(requestUrl);
    }

    public deleteQuestion = (question: IQuestion): AxiosPromise<number> => {
        const requestUrl = 'http://localhost/rest/questionService.php?deleteQuestion';
        return axios.post(requestUrl, question);
    }
}