# Unity-Backoffie
React project to display a backoffice tool for surveys with TypeScript, webpack, SCSS module support and browser-sync.

## How to use

Simply clone this repo by using
```
  git clone <MyProject>
```
  
then move on to remove the .git folder and simply run
```
  git init
```
  
to initialize a new git repository.

Afterwards install all dependencies by running
```
npm i
```

## Usage

This project contains two scripts in the package.json, one for development, one for production build.

### Development

Just run 
```
npm start
```

and a browser window with browser-sync should open.

### Production
Just run
```
npm run prod
```

to get a minified, production-ready bundle in the "dist" folder.
